FROM zaproxy/zap-stable

MAINTAINER Chris Runo <chris.runo@mediacurrent.com>

ENV LC_ALL=C.UTF-8

USER root
CMD /bin/bash

RUN DEBIAN_FRONTEND=noninteractive apt-get -y install;

# Replace shell with bash so we can source files and install packages.
RUN rm /bin/sh && ln -s /bin/bash /bin/sh && \
    apt-get update && \
    apt-get -y upgrade; \
    apt-get -y dist-upgrade && \
    apt-get -y install apt-transport-https lsb-release ca-certificates curl && \
    curl -sSLo /usr/share/keyrings/deb.sury.org-php.gpg https://packages.sury.org/php/apt.gpg && \
    sh -c 'echo "deb [signed-by=/usr/share/keyrings/deb.sury.org-php.gpg] https://packages.sury.org/php/ $(lsb_release -sc) main" > /etc/apt/sources.list.d/php.list' && \
    apt-get update --fix-missing;

# Install basic packages
RUN apt-get -q -y install --fix-missing \
    apache2 \
    autoconf \
    build-essential \
    bzip2 \
    file \
    gcc \
    imagemagick \
    jq \
    libc-dev \
    libfontconfig \
    libfreetype6 \
    libssl-dev \
    make \
    patch \
    pkg-config \
    rsync \
    python3-venv \
    default-mysql-client \
    vim ;

# Install PHP8.2
RUN apt-get -y install \
    libapache2-mod-php8.2 \
    php8.2 \
    php8.2-bcmath \
    php8.2-cli \
    php8.2-common \
    php8.2-curl \
    php8.2-dev \
    php8.2-gd \
    php8.2-intl \
    php8.2-mbstring \
    php8.2-mysql \
    php8.2-soap \
    php8.2-xml \
    php8.2-zip \
    php8.2-imagick \
    php8.2-memcache \
    php-pear;

# Disable "expose_php" setting
RUN bash -c 'echo "expose_php = Off" >> /etc/php/8.2/cli/php.ini' && \
    bash -c 'echo "expose_php = Off" >> /etc/php/8.2/apache2/php.ini' && \
    php -i | grep "expose_php";

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/bin;
RUN composer --verbose self-update --stable;

# Install Chromium browser and Chrome dependencies
RUN apt-get install -y \
    chromium \
    gconf-service \
    libasound2 \
    libatk1.0-0 \
    libc6 \
    libcairo2 \
    libcups2 \
    libdbus-1-3 \
    libexpat1 \
    libfontconfig1 \
    libgcc1 \
    libgconf-2-4 \
    libgdk-pixbuf2.0-0 \
    libglib2.0-0 \
    libgtk-3-0 \
    libnspr4 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libstdc++6 \
    libx11-6 \
    libx11-xcb1 \
    libxcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxss1 \
    libxtst6 \
    fonts-liberation \
    libappindicator1 \
    libnss3 \
    lsb-release \
    xdg-utils \
    wget;


# Install node and npm
ENV NVM_DIR $HOME/.nvm
ENV NODE_VERSION 20.16.0
RUN curl --silent -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash && \
    source $NVM_DIR/nvm.sh && \
    nvm install $NODE_VERSION && \
    nvm alias default $NODE_VERSION && \
    nvm use default;

# Add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

# Install NPM modules
RUN npm install -g pa11y && \
    npm install -g lighthouse

# Install AWS cli
RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" && \
    unzip awscli-bundle.zip && \
    ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws;

# Install acli
RUN curl -OL https://github.com/acquia/cli/releases/latest/download/acli.phar && \
    chmod +x acli.phar && \
    mv acli.phar /usr/local/bin/acli

# Clean up installs
RUN apt-get autoremove -y && \
    apt-get clean -y;
RUN rm -rf /root/.composer /tmp/* ./awscli-bundle

# Link the ssh config for root for the zap user
RUN ln -sf /home/zap/.ssh /root/.ssh

# Add the slackcli script
COPY assets/slackcli /usr/local/bin/

CMD bash
