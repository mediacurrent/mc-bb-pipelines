FROM owasp/zap2docker-stable

MAINTAINER Chris Runo <chris.runo@mediacurrent.com>

ENV LC_ALL=C.UTF-8

USER root
CMD /bin/bash

RUN DEBIAN_FRONTEND=noninteractive apt-get -y install;

# Replace shell with bash so we can source files and install packages.
RUN rm /bin/sh && ln -s /bin/bash /bin/sh && \
    apt-get update && \
    apt-get -y upgrade; \
    apt-get -y dist-upgrade && \
    apt-get install -y software-properties-common && \
    add-apt-repository -y ppa:ondrej/php && \
    apt-get update --fix-missing;

# Install basic packages
RUN apt-get -q -y install --fix-missing \
    apache2 \
    autoconf \
    build-essential \
    bzip2 \
    ca-certificates \
    file \
    gcc \
    imagemagick \
    libc-dev \
    libfontconfig \
    libfreetype6 \
    libssl-dev \
    make \
    patch \
    pkg-config \
    rsync \
    python3-venv \
    mysql-client \
    vim ;

# Install PHP7
RUN apt-get -y install \
    libapache2-mod-php7.4 \
    php7.4 \
    php7.4-bcmath \
    php7.4-cli \
    php7.4-curl \
    php7.4-dev \
    php7.4-gd \
    php7.4-intl \
    php7.4-json \
    php7.4-mbstring \
    php7.4-mysql \
    php7.4-soap \
    php7.4-xml \
    php7.4-zip \
    php-imagick \
    php-memcache \
    php-pear;

# Disable "expose_php" setting
RUN bash -c 'echo "expose_php = Off" >> /etc/php/7.4/cli/php.ini' && \
    bash -c 'echo "expose_php = Off" >> /etc/php/7.4/apache2/php.ini' && \
    php -i | grep "expose_php";

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --filename=composer --install-dir=/usr/bin;
RUN composer --verbose self-update --stable;

# Install Chromium browser and Chrome dependencies
RUN apt-get install -y \
    chromium-browser \
    gconf-service \
    libasound2 \
    libatk1.0-0 \
    libc6 \
    libcairo2 \
    libcups2 \
    libdbus-1-3 \
    libexpat1 \
    libfontconfig1 \
    libgcc1 \
    libgconf-2-4 \
    libgdk-pixbuf2.0-0 \
    libglib2.0-0 \
    libgtk-3-0 \
    libnspr4 \
    libpango-1.0-0 \
    libpangocairo-1.0-0 \
    libstdc++6 \
    libx11-6 \
    libx11-xcb1 \
    libxcb1 \
    libxcomposite1 \
    libxcursor1 \
    libxdamage1 \
    libxext6 \
    libxfixes3 \
    libxi6 \
    libxrandr2 \
    libxrender1 \
    libxss1 \
    libxtst6 \
    fonts-liberation \
    libappindicator1 \
    libnss3 \
    lsb-release \
    xdg-utils \
    wget;


# Install node and npm
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 16.19.0
RUN curl --silent -o- https://raw.githubusercontent.com/creationix/nvm/v0.39.2/install.sh | bash && \
    source $NVM_DIR/nvm.sh && \
    nvm install $NODE_VERSION && \
    nvm alias default $NODE_VERSION && \
    nvm use default;

# Add node and npm to path so the commands are available
ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH

RUN npm config set user 0 && \
    npm config set unsafe-perm true;

# Install NPM modules
RUN npm install -g pa11y@5.3.1 && \
    npm install -g lighthouse && \
    npm install -g forever && \
    npm install -g backstopjs;

# Install AWS cli
RUN curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip" && \
    unzip awscli-bundle.zip && \
    ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws;

# Clean up installs
RUN apt-get autoremove -y && \
    apt-get clean -y;
RUN rm -rf /root/.composer /tmp/* ./awscli-bundle

# Link the ssh config for root for the zap user
RUN ln -sf /home/zap/.ssh /root/.ssh

# Add the slackcli script
COPY assets/slackcli /usr/local/bin/

CMD bash
